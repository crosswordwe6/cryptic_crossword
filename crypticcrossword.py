LF = '\n'
SIZE = 15
def update(pattern: str) -> list[str]:
   cryptic_pattern = pattern.split(LF)
   cryptic_pattern = ['B' * SIZE] + cryptic_pattern + ['B' * SIZE]
   return ['B' + each + 'B' for each in cryptic_pattern]

def assign_numbers(cryptic_pattern: list[list]) -> list[list]:
   clue_positions = []
   num = 1
   pattern = update(cryptic_pattern)
   for row_pos, row in enumerate(pattern[1:-1], start = 1):
      for col_pos, col in enumerate(row[1:-1], start = 1):
          if col == 'W':
              if row[col_pos - 1] == 'B' and row[col_pos + 1] == 'W':
                  clue_positions.append((row_pos,col_pos, num))
                  num += 1
              elif pattern[row_pos-1][col_pos] == 'B' and pattern[row_pos + 1][col_pos] == 'W':
                  clue_positions.append((row_pos,col_pos,num))
                  num += 1

   return clue_positions

pattern =\
'''WWWWWWWWBWWWWWW
WBWBWBWBBBWBWBW
WWWWWWWWBWWWWWW
WBWBWBWBWBWBWBW
WWWWWWWWWWWBBBB
WBWBBBWBWBWBWBW
WWWWWBWWWWWWWWW
WBWBWBWBWBWBWBW
WWWWWWWWWBWWWWW
WBWBWBWBWBBBWBW
BBBBWWWWWWWWWWW
WBWBWBWBWBWBWBW
WWWWWWBWWWWWWWW
WBWBWBBBWBWBWBW
WWWWWWBWWWWWWWW'''

for each in assign_numbers(pattern):
  print(each)
             

   
